GRConverter
=======

File format support for Maemo's [GPSRecorder](https://github.com/polyvertex/gpsrecorder-n900).

Building for N900
--------------------

```
podman run --name grconverter_n900 -v.:/mnt/src:z -ti fedora:latest /bin/bash
```

The N900 has an ARMv7A CPU, but Linux is built as "armel".

The kernel is 2.6, meaning that Rust won't support old enough libc ABI. Thankfully, Rust supports musl libc, which supports kernel version 2.6.

```
dnf install -y gcc # needed for syn at compile time
dnf install -y lld # no armel gcc toolchain for Fedora, so use clang linker cause it does armel out of the box
curl https://sh.rustup.rs -sSf | sh -s -- -t armv7-unknown-linux-musleabi
cd /mnt/src/grconverter
source $HOME/.cargo/env
cargo build --target armv7-unknown-linux-musleabi
```

This does not support any shared libraries on the N900. For more info look here: https://burgers.io/cross-compile-rust-from-arm-to-x86-64
