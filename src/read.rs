use crate::packed;
use std::io::Read;


use std::convert::TryInto;


#[derive(Debug)]
pub enum Part {
    Header(packed::Header),
    Chunk(packed::Chunk),
}

pub fn readstream<'a>(mut s: &'a mut impl Read)
    -> impl FnMut() -> Option<Result<Part, packed::ReadError>> + 'a
{
    #[derive(PartialEq)]
    enum Phase {
        Header,
        Chunk,
        End,
    }

    let mut phase = Phase::Header;
    move || {
        let (new_phase, result) = match phase {
            Phase::End => (
                Phase::End,
                Some(Err(packed::ReadError::Msg("Done")))
            ),
            Phase::Header => (
                Phase::Chunk, 
                Some(
                    packed::Header::from_stream(&mut s)
                        .map(Part::Header)
                )
            ),
            Phase::Chunk => {
                let chunk = packed::RawChunk::from_stream(&mut s);
                match chunk {
                    Ok(Some(chunk)) => match chunk.try_into() {
                        Err(msg) => {
                            eprintln!("Skipping chunk: {}", msg);
                            (Phase::Chunk, None)
                        },
                        Ok(chunk) => (Phase::Chunk, Some(Ok(Part::Chunk(chunk)))),
                    },
                    Ok(None) => (Phase::End, Some(Err(packed::ReadError::Msg("Data over")))),
                    Err(e) => (Phase::End, Some(Err(e))),
                }
            }
        };
        phase = new_phase;
        result
    }
}
