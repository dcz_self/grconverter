use num::{BigRational, Zero, Signed};
use std::collections::HashMap;


use std::fmt::Write;


pub fn big_rational_to_string(mut n: BigRational) -> String {
    let neg = if n.is_negative() {
        n = -n;
        true
    } else {
        false
    };
    let mut fraction = String::new();
    let mut map = HashMap::new();
    let mut rem = n.numer() % n.denom();
    while !rem.is_zero() && !map.contains_key(&rem) {
        map.insert(rem.clone(), fraction.len());
        rem *= 10;
        fraction.push_str(&(rem.clone() / n.denom()).to_string());
        rem = rem % n.denom();
    }
    let mut output = if neg {
        "-".to_owned()
    } else {
        String::new()
    };
    output.push_str(&(n.numer() / n.denom()).to_string());
    
    // Panic. If the output string can't get formatted, something is *really* wrong.
    if rem.is_zero() {
        if fraction.len() != 0 {
            write!(output, ".{}", &fraction).unwrap();
        }
    } else {
        write!(output, ".{}({})", &fraction[..map[&rem]], &fraction[map[&rem]..]).unwrap();
    }
    output
}
