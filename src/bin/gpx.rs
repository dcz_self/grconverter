use grconverter::decimal_str::big_rational_to_string;
use grconverter::read;
use grconverter::packed;

use num_rational::BigRational;

use std::env;
use std::fs::File;
use std::io;
use std::io::BufReader;
use std::path::Path;

use time::OffsetDateTime;


use std::io::Write;


pub trait Consumator<T> {
    type NextResult;
    type CollectResult;
    /// Feeds another item.
    fn next(&mut self, value: T) -> Self::NextResult;
    /// Ends processing, returning the result.
    fn collect(self) -> Self::CollectResult;
}

enum Chunk {
    Fix(packed::LocationFix),
    NewTrack,
    FixLost, // Unsupported
}

impl From<packed::Chunk> for Chunk {
    fn from(chunk: packed::Chunk) -> Self {
        use packed::ChunkData::*;
        match chunk.data {
            LocationFix(locationfix) => Chunk::Fix(locationfix),
            NewTrack(newtrack) => Chunk::NewTrack,
            LocationFixLost => Chunk::FixLost,
        }
    }
}

fn ts_to_time(ts: i64) -> String {
    OffsetDateTime::from_unix_timestamp(ts).format("%FT%T")
}

fn fix_format(fix: packed::LocationFix) -> String {
    format!(
        "
            <trkpt lat=\"{lat}\" lon=\"{lon}\">
                <time>{time_utc}Z</time>
                <fix>{fixtype}</fix>
                <ele>{elevation}</ele>
                <speed>{speed}</speed>
                <sat>{sat_used}</sat>
                <extensions>
                    <hdopCM>{hdop_cm}</hdopCM>
                    <vdopCM>{vdop_cm}</vdopCM>
                    <sdop>{sdop}</sdop>
                </extensions>
            </trkpt>",
        lat = big_rational_to_string(BigRational::new(
            fix.data.latitude.into(),
            packed::LOCFIX_MULTIPLIER_LATLONG.into(),
        )),
        lon = big_rational_to_string(BigRational::new(
            fix.data.longitude.into(),
            packed::LOCFIX_MULTIPLIER_LATLONG.into(),
        )),
        time_utc = ts_to_time(fix.data.time as i64),
        fixtype = match fix.data.fix_mode {
            packed::FixMode::_2D => "2d",
            packed::FixMode::_3D => "3d",
            _ => "none",
        },
        // fixme: missing fields
        elevation = "",
        speed = "",
        sat_used = fix.data.sat_use,
        hdop_cm = fix.data.horizontal_uncertainty,
        vdop_cm = fix.data.vertical_uncertainty,
        sdop = fix.data.speed_uncertainty,
    )
}

enum GpxState {
    Header,
    Track,
    FixOrEnd,
}

struct GpxConsumator(GpxState);

impl GpxConsumator {
    fn new() -> Self {
        GpxConsumator(GpxState::Header)
    }
    fn next(&mut self, value: read::Part) -> String {
        use read::Part::*;
        use read::Part;
        match (&self.0, value) {
            (&GpxState::Header, Header(h)) => {
                self.0 = GpxState::Track;
                // FIXME: add timezone info
                format!(
                    "<?xml version=\"1.1\" ?>
<gpx version=\"1.1\" creator=\"grconverter\" xmlns=\"http://www.topografix.com/GPX/1/1\" xmlns:maemo=\"types that Maemo supports\">
    <metadata>
        <time>{utc_time}Z</time>
    </metadata>",
                    utc_time = ts_to_time(h.time as i64),
                )
            },
            (&GpxState::Track, Part::Chunk(c)) => match c.data {
                packed::ChunkData::LocationFix(fix) => {
                    self.0 = GpxState::FixOrEnd;
                    format!(
                        "
    <trk>
        <src>Nokia N900</src>
        <trkseg>") + &fix_format(fix)
                },
                _ => "".into(), //needs track, found unsupported data\n".into(),
            },
            (&GpxState::FixOrEnd, Part::Chunk(c)) => match c.data {
                packed::ChunkData::LocationFix(fix) => fix_format(fix),
                _ => "".into(), // "needs fix, found unsupported data\n".into(),
            },
            (&GpxState::Header, Part::Chunk(c)) => panic!("Expected header, got chunk {:#?}", c),
            (_, Header(_)) => panic!("Got extra header"),
        }
    }

    fn collect(self) -> String {
        match self.0 {
            GpxState::Header => "".into(),
            GpxState::Track => "
</gpx>".into(),
            GpxState::FixOrEnd => "
        </trkseg>
    </trk>
</gpx>".into(),
        }
    }
}

/*
struct Header {
    format: u8, // version
    // uninteresting
    time: u32, // posix timestamp utc
    tz_offset: i32, // seconds
}
impl Header {
    
}*/
/*
enum Chunk {
    NewTrack(NewTrack),
}*/

fn convert_one(in_path: &Path, out_path: &Path) -> io::Result<()> {
    let inf = File::open(in_path)?;
    let mut reader = BufReader::new(inf);
    let mut stream = read::readstream(&mut reader);
    let mut outf = File::create(out_path)?;
    let mut gpx = GpxConsumator::new();
    loop {
        match stream() {
            None => {},
            Some(v) => match v {
                Ok(i) => {
                    write!(outf, "{}", gpx.next(i)).unwrap();
                },
                Err(packed::ReadError::Msg("Data over")) => {},
                Err(e) => {
                    eprintln!("{:?}, {:?}", in_path, e);
                    break;
                },
            },
        }
    }
    write!(outf, "{}", gpx.collect()).unwrap();
    Ok(())
}

fn main() {
    let paths = env::args().skip(1);
    for path in paths {
        let in_path = Path::new(&path);
        convert_one(&in_path, &in_path.with_extension("gpx"))
            .unwrap_or_else(|e| eprintln!("{} encountered an error: {}", in_path.display(), e));
    }
}
