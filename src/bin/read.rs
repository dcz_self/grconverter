use grconverter::read::readstream;

use std::env;
use std::fs::File;
use std::io::BufReader;

fn main() {
    let f = File::open(&env::args().collect::<Vec<_>>()[1]).unwrap();
    let mut reader = BufReader::new(f);
    let mut stream = readstream(&mut reader);
    loop {
        match stream() {
            None => {
                println!("Skipped");
            },
            Some(Ok(i)) => {
                println!("{:#?}", i);
            },
            Some(Err(e)) => {
                println!("{:?}", e);
                break;
            }
        }
    }
}
