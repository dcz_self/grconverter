use std::convert::TryFrom;
use std::io;
use std::io::Read;
use packed_struct::prelude::*;
use packed_struct::types::bits::ByteArray;


use std::convert::TryInto;


/// Almost like read_exact, but returns how many bytes were definitely read.
fn read_exact(this: &mut impl Read, mut buf: &mut [u8]) -> Result<(), (usize, io::Error)> {
    let mut readcount = 0;
    while !buf.is_empty() {
        match this.read(buf) {
            Ok(0) => break,
            Ok(n) => {
                readcount += n;
                let tmp = buf;
                buf = &mut tmp[n..];
            }
            Err(ref e) if e.kind() == io::ErrorKind::Interrupted => {}
            Err(e) => return Err((readcount, e)),
        }
    }
    if !buf.is_empty() {
        Err((readcount, io::Error::new(io::ErrorKind::UnexpectedEof, "failed to fill whole buffer")))
    } else {
        Ok(())
    }
}


#[derive(PackedStruct, Debug)]
#[packed_struct(endian="lsb")]
pub struct Header {
    magic: [u8; 4], // b"GPSR"
    format: u8, // version
    pub time: u32, // posix timestamp utc
    pub tz_offset: i32, // seconds
}

impl Header {
    pub fn from_stream(s: &mut impl Read) -> Result<Self, ReadError> {
        let mut buf = [0; 13];
        read_exact(s, &mut buf).map_err(|(position, e)| ReadError::Io(position, e))?;
        let header = Header::unpack(&buf).unwrap();

        if &header.magic != b"GPSR" {
            return Err(ReadError::Msg("Bad magic"));
        }
        Ok(header)
    }
}

#[derive(PrimitiveEnum_u16, Debug, Clone, Copy)]
pub enum ChunkType {
    ChunkLocationFix = 20,
    ChunkLocationFixLost = 21,
    ChunkPaused = 40,
    ChunkResumed = 41,
    ChunkNewTrack = 45,
}    

#[derive(PackedStruct, Debug)]
#[packed_struct(endian="lsb")]
pub struct ChunkHeader {
    magic: u8, // '@'
    #[packed_field(size_bytes="2", ty="enum")]
    pub id: ChunkType,
    size: u32, // including header
    time: u32, // posix utc timestamp
    //data: Vec<u8>,
}

#[derive(Debug)]
pub enum ReadError {
    Io(usize, io::Error),
    Msg(&'static str),
    Packing(PackingError),
}

#[derive(Debug)]
pub struct RawChunk {
    header: ChunkHeader,
    data: Vec<u8>,
}

impl RawChunk {
    pub fn from_stream(s: &mut impl Read) -> Result<Option<Self>, ReadError> {
        let mut buf = [0; 11];
        match read_exact(s, &mut buf) {
            Ok(_) => {},
            Err((0, e)) => match e.kind() {
                io::ErrorKind::UnexpectedEof => { return Ok(None) },
                _ => panic!("{}", e),
            },
            Err((other, e)) => return Err(ReadError::Io(other, e)),
        };
        let chunk_header = ChunkHeader::unpack(&buf).map_err(ReadError::Packing)?;

        if chunk_header.magic != b'@' {
            return Err(ReadError::Msg("Bad magic"));
        }
        let data_size = chunk_header.size as usize - <ChunkHeader as PackedStruct>::ByteArray::len();
        //println!("{} {} {}", data_size, chunk_header.size, <ChunkHeader as PackedStruct>::ByteArray::len());
        let mut data = Vec::with_capacity(data_size);
        data.resize(data_size, 0);
        read_exact(s, &mut data).map_err(|(place, e)| ReadError::Io(place, e))?;

        Ok(Some(RawChunk {
            header: chunk_header,
            data,
        }))
    }
}

#[derive(Debug)]
pub struct Chunk {
    header: ChunkHeader,
    pub data: ChunkData,
}

impl TryFrom<RawChunk> for Chunk {
    type Error = &'static str;
    fn try_from(chunk: RawChunk) -> Result<Self, Self::Error> {
        let data = match chunk.header.id {
            ChunkType::ChunkNewTrack => ChunkData::NewTrack(NewTrack {
                tz_offset: TzOffset::unpack(&chunk.data[0..4].try_into().unwrap()).unwrap(),
                name: chunk.data[4..].into(),
            }),
            ChunkType::ChunkLocationFixLost => ChunkData::LocationFixLost, // FIXME: make sure that data is consumed
            ChunkType::ChunkLocationFix => {
                let start = 0;
                let size = <LocationFixData as PackedStruct>::ByteArray::len();
                let end = start + size;
                let data = LocationFixData::unpack_from_slice(
                    &chunk.data[start..end]
                ).unwrap();

                ChunkData::LocationFix(LocationFix {
                    data,
                    satellites: Vec::new(), // FIXMe
                })
            },
            ChunkType::ChunkPaused => {
                // Not really an error, just unsupported.
                return Err("Pause unsupported");
            },
            ChunkType::ChunkResumed => {
                // Not really an error, just unsupported.
                return Err("Resume unsupported");
            },
        };
        Ok(Chunk {
            header: chunk.header,
            data,
        })
    }
}

#[derive(PackedStruct, Debug)]
#[packed_struct(endian="lsb")]
pub struct TzOffset {
    seconds: u32,
}

#[derive(Debug)]
pub struct NewTrack {
    tz_offset: TzOffset,
    name: Vec<u8>,
}

#[allow(non_snake_case)]
#[derive(PackedStruct, Debug)]
#[packed_struct(endian="lsb")]
pub struct LocationFixCellInfoGsm {
    setup: u8,
    MCC: u16, // Mobile Country Code
    MNC: u16, // Mobile Network Code
    LAC: u16, // Location Area Code
    cell_id: u16,
}

#[allow(non_snake_case)]
#[derive(PackedStruct, Debug)]
#[packed_struct(endian="lsb")]
pub struct LocationFixCellInfoWcdma {
    setup: u8,
    MCC: u16, // Mobile Country Code
    MNC: u16, // Mobile Network Code
    cell_id: u16,
}

pub const LOCFIX_MULTIPLIER_LATLONG: i32 = 1000000;
//pub const LOCFIX_MULTIPLIER_TRACK   = 100,
//pub const LOCFIX_MULTIPLIER_SPEED   = 1000, // km/h -> m/h
//pub const LOCFIX_MULTIPLIER_CLIMB   = 100,  // m/s -> cm/s

#[derive(PrimitiveEnum_u8, Debug, Clone, Copy)]
pub enum FixMode {
    NotSeen = 0,
    NoFix = 1,
    _2D = 2,
    _3D = 3,
}

#[derive(PackedStruct, Debug)]
#[packed_struct(endian="lsb")]
pub struct LocationFixData {
    #[packed_field(size_bytes="1", ty="enum")]
    pub fix_mode: FixMode,
    fix_fields: u16, // one or several FIXFIELD_* flags
    pub time: u32, // posix timestamp
    time_uncertainty: u32, // estimated time uncertainty (in seconds)
    pub latitude: i32, // latitude ; quint32(lat * LOCFIX_MULTIPLIER_LATLONG)
    pub longitude: i32, // longitude ; quint32(long * LOCFIX_MULTIPLIER_LATLONG)
    pub horizontal_uncertainty: u32, // horizontal position uncertainty (cm) ; quint32(eph)
    altitude: i32, // altitude (m) ; qint32(altm)
    pub vertical_uncertainty: u32, // vertical position uncertainty (m) ; quint32(epv)
    heading: u16, // heading in [0;359] degrees * LOCFIX_MULTIPLIER_TRACK ; quint16(track * LOCFIX_MULTIPLIER_TRACK)
    heading_uncertainty: u16, // track uncertainty in degrees * LOCFIX_MULTIPLIER_TRACK ; quint16(epd * LOCFIX_MULTIPLIER_TRACK)
    pub speed: u32, // speed in m/h ; quint32(speed * LOCFIX_MULTIPLIER_SPEED) [horizontal?]
    pub speed_uncertainty: u32, // speed uncertainty in m/h ; quint32(eps *  LOCFIX_MULTIPLIER_SPEED)
    upwards_speed: i16, // rate of climb in [+-]cm/s ; qint16(climb * LOCFIX_MULTIPLIER_CLIMB)
    upwards_speed_uncertainty: u16, // climb uncertainty in cm/s ; quint16(epc * LOCFIX_MULTIPLIER_CLIMB)
    // cell info
    #[allow(non_snake_case)]
    #[packed_field(size_bytes="9")]
    GSM: LocationFixCellInfoGsm,
    #[allow(non_snake_case)]
    #[packed_field(size_bytes="7")]
    WCDMA: LocationFixCellInfoWcdma,

    // satellites count
    sat_count: u8, // number of satellites structures appended to this fix
    pub sat_use: u8, // number of satellites actually used to calculate this fix
}

#[derive(Debug)]
pub struct Satellite {
    in_use: u8, // is this satellite used to calculate this fix ?
    PRN: i32, // id
    elevation: u8, // elevation in [0;90] degrees
    azimuth: u16, // azimuth in [0;359] degrees
    snr: u8, // signal to noise ratio in [0;99] dBHz
}

#[derive(Debug)]
pub struct LocationFix {
    pub data: LocationFixData,
    satellites: Vec<Satellite>,
}

#[derive(Debug)]
pub enum ChunkData {
    NewTrack(NewTrack),
    LocationFix(LocationFix),
    LocationFixLost,
}
